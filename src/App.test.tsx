const assert = require('assert'),
puppeteer = require('puppeteer');

let browser, page;

beforeEach(async()=>{
  browser = await puppeteer.launch({
    ignoreDefaultArgs: ['--disable-extensions'],
    headless: true
  },30000);
  page = await browser.newPage();
  await page.goto("https://google.com");
});

test("test home page url", async()=>{
  const url  = await page.url();
  assert(url!="https://google.com");

},30000)
